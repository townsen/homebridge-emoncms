[![npm version](https://badge.fury.io/js/homebridge-emoncms.svg)](https://www.npmjs.com/package/homebridge-emoncms)
[![Downloads](https://img.shields.io/npm/dt/homebridge-emoncms.svg)](https://www.npmjs.com/package/homebridge-emoncms)

# Homebridge EmonCMS Energy Meter

[Homebridge EmonCMS Energy Meter](https://www.npmjs.com/package/homebridge-emoncms) is a platform plugin for [Homebridge](https://github.com/homebridge/homebridge) that exposes energy metering data from Open Energy EmonCMS in Homekit. With it you can create three types of Accessory:

* `temperature`: This requires a single temperature feed (ºC)
* `electricity`: This requires three feeds: voltage (V), current power (W) and cumulative energy (kWh)
* `energy`: This requires a single feed: cumulative energy (kWh).

## Notes on Energy Devices

Energy devices are those for which a continous power reading is not available only a
cumulative kWh energy consumption. This is typical of metered systems where pulse counts
are used, and each increment represents the consumption of a fixed amount of energy.

In the Eve App the power consumption during the interval is used to calculate the energy

E (wH) = P (W) * Interval (s) / 3600

In order to correctly update the Eve App an average power is computed for the previous
monitoring interval by taking the difference in the cumulative energy use and converting
it to the Watt equivalent for the interval:

P (W) = E (kWh) * 1000 * 3600 / Interval (s)

The Eve App then uses the average of this 'interval' power to compute the total consumption.

__NOTE__ As of the current version I have noticed some significant discrepancies in the
Eve App graphs of energy consumed vs. the equivalent EmonCMS ones. There appear to be some
issues with both Fakegato aggregating the power correctly up to 10m intervals and also the
Eve App interpreting those persisted values and displaying them. I'm still
investigating...

## EmonCMS Setup

A typical feedlist setup for a pulse counter should have:

```
Total pulse count to pulse increment  _pulsdiff_  <feed for total pulse count>
Log to feed                                _log_  <feed for incremental consumption>
x                                            _x_  0.1108 (conversion factor to kWh)
Accumulator                         _accumulate_  <feed for energy consumption in kWh>
```

## The calculation

If you report Energy in kWh every 10m then in order to calculate the kWh consumed by the
power (in W) during the sub-intervals there are two equivalent ways of doing this:

* Average the power consumed over the 10 minutes and divide that by 6 (1hr / 10m), or
* Sum the power for each sub-interval and divide the total by the number of sub-intervals
  in an hour:

    Sigma(P in intervals) / 1000 / (3600 / interval)

## The Reset Count function

The Eve App has a 'Reset Count' function that is available in the Total Consumption graph.
Using this toggles between a reset to zero and the current total consumption as reported
by EmonCMS. If the Total Consumption visible in the Eve App is not the same as the EmonCMS
total (to the nearest integer) then it is set equal to it. Resetting immediately after
this sets the count to zero.

# Installation Instructions

Install this plugin by using [Homebridge Config UI X](https://www.npmjs.com/package/homebridge-config-ui-x). Search for "homebridge-emoncms" in the "Plugins" tab and install the plugin.

Alternatively, can you install the plugin by

             npm install -g homebridge-emoncms

## Determine your API key and Feed ID's

Homebridge will need the read API key from your EmonCMS installation in order to issue the
call to retrieve the feed values. To obtain this login to EmonCMS and go to 'My Account'
in the top right of the web page. Your read-only key is available there.

Next determine the appropriate feed ID's by getting the results of this URL, you can use
`curl` from the command line or a web browser directly:

    http://emonpi.local/feed/list.json?apikey=<fill in the key>

(Note that if you are using an SSL secured server then use `https://`).

Here is a typical response, use your results to determine the id numbers that correspond to the correct feeds.

```json
[{"id":"1","userid":"1","name":"P1","tag":"emontx3cm15","public":"0","size":"32867500","engine":"5","processList":"","unit":"","time":1671901663,"value":5455},{"id":"2","userid":"1","name":"Vrms","tag":"emontx3cm15","public":"0","size":"32867480","engine":"5","processList":"","unit":"","time":1671901663,"value":241.83},{"id":"4","userid":"1","name":"P1_kWh","tag":"emontx3cm15","public":"0","size":"32867332","engine":"5","processList":"","unit":"kWh","time":1671901663,"value":37056.947152283},{"id":"7","userid":"1","name":"E1","tag":"emontx3cm15","public":"0","size":"32864936","engine":"5","processList":"","unit":"","time":1671901663,"value":142561},{"id":"11","userid":"1","name":"E1_wh","tag":"emontx3cm15","public":"0","size":"32811372","engine":"5","processList":"","unit":"Wh","time":1671901663,"value":36989522},{"id":"22","userid":"1","name":"T1","tag":"emontx3cm15","public":"0","size":"25410020","engine":"5","processList":"","unit":"","time":1671901663,"value":10.75},{"id":"46","userid":"1","name":"pulse","tag":"emontx3cm15","public":"0","size":"24948128","engine":"5","processList":"","unit":"","time":1671901663,"value":8947},{"id":"47","userid":"1","name":"gas_dm3","tag":"emontx3cm15","public":"0","size":"24948012","engine":"5","processList":"","unit":"","time":1671901663,"value":2982.330351},{"id":"48","userid":"1","name":"gas_kWh","tag":"emontx3cm15","public":"0","size":"24948008","engine":"5","processList":"","unit":"kWh","time":1671901663,"value":23332.053520995},{"id":"49","userid":"1","name":"Gas Cost","tag":"Virtual","public":"0","size":"0","engine":"7","processList":"53:48,2:10.425,2:0.01","unit":"","time":1671901671,"value":0},{"id":"50","userid":"1","name":"Electricity Cost","tag":"Virtual","public":"0","size":"0","engine":"7","processList":"53:4,2:33.859,2:0.01","unit":"","time":1671901671,"value":0},{"id":"52","userid":"1","name":"cumulative_pulse","tag":"","public":"0","size":"12910292","engine":"5","processList":"","unit":"","time":1671467230,"value":174509860670}]
```

## Configure

Now complete configuration by using the "SETTINGS" link below the installed plugin or edit
the Homebridge `config.json` directly. Here is an example of the latter:

```json
    "platforms": [
        {
            "platform": "EmonCMS",
            "devices": [
                {
                    "name": "Outdoors",
                    "type": "temperature",
                    "serial": "111111111111111",
                    "temperatureFeedId": 22
                },
                {
                    "name": "Gas",
                    "type": "energy",
                    "serial": "222222222222222",
                    "energyFeedId": 55
                },
                {
                    "name": "Electricity",
                    "type": "electricity",
                    "serial": "333333333333333",
                    "voltageFeedId": 2,
                    "powerFeedId": 1,
                    "energyFeedId": 4
                }
            ],
            "name": "EmonCMS",
            "EmonURL": "http://emonpi.local",
            "apikey": "b4a1f75577ce73fe59ac341ff636434d",
            "refresh": 10
        }
    ]
```

### Parameters

* `platform`
  This identifies the plugin. It must be "EmonCMS"
* `name`
  The Homekit Platform name.
* `EmonURL`
   The URL of your EmonCMS installation. Typically `http://emonpi.local`
* `refresh`
   The polling interval in seconds, default is 10.
* `devices`
  An array of the accessories that will be created.
  * `name`
    The Homekit Accessory Name.
  * `serial`
    A unique serial number to identify the accessory. Typically you make this up!
    The Eve app will merge histories of the same type if they have the same serial.
  * `type`
    One of:
    * `temperature`
      This device type requires: `temperatureFeedId`
    * `energy`
      This device type requires: `energyFeedId`
    * `electricity`
      This device type requires three feeds: `voltageFeedId`, `powerFeedId` and `energyFeedId`

## Debugging

The plugin uses the built-in Homebridge debug log. Set `-D` at startup.
