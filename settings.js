'use strict';
// The name used to register the platform in the Homebridge configuration
exports.PLATFORM_NAME = 'EmonCMS';
// The name of the plugin as defined in package.json
exports.PLUGIN_NAME = 'homebridge-emoncms';
// The UUID for an Eve Energy Service
exports.uuidEveEnergy = '00000001-0000-1777-8000-775D67EC4377';
// The UUID for FakeGatoHistory Service
exports.uuidFakeGatoHistoryService = 'E863F007-079E-48FF-8F27-9C2605A29F52';
