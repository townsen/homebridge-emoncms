'use strict';

const fs = require('node:fs');
const moment = require('moment');
const ci = require('correcting-interval');
const fetch = require('node-fetch');
const settings = require('./settings');
const EmonCMSPlatformAccessory = require('./platformAccessory').EmonCMSPlatformAccessory;

/**
 * HomebridgePlatform
 * This class is the main constructor for your plugin, this is where you should
 * parse the user config and discover/register accessories with Homebridge.
 */
class EmonCMS {

  constructor(log, config, api) {

    this.validateConfiguration(config);

    this.log = log;
    this.config = config;
    this.api = api;
    this.Service = this.api.hap.Service;
    this.Characteristic = this.api.hap.Characteristic;
    this.FakeGatoHistory = require('fakegato-history')(api); // The class for History services

    this.cachedAccessories = [];
    this.deregisterAccessories = []; // Accessories restored from cache that are no longer in config
    this.accessories = new Map(); // key is the PlatformAccessory, value is the EmonCMSPlatformAccessory
    this.EmonURL = config['EmonURL'];
    this.apikey = config['apikey'];
    this.name = config['name'];
    this.refresh = Number(config['refresh'] || 10);
    this.timeLastUpdate = 0;

    this.log.debug('Finished initializing platform:', this.config.name);

    this.feedValues = new Map(); // keyed by feed ID
    // loop over the configured devices and record their feeds
    for (const details of this.config.devices) {
      switch (details.type) {
        case 'electricity':
          this.feedValues.set(details.voltageFeedId, 0.0);
          this.feedValues.set(details.powerFeedId, 0.0);
          this.feedValues.set(details.energyFeedId, 0.0);
          break;
        case 'energy':
          this.feedValues.set(details.energyFeedId, 0.0);
          break;
        case 'temperature':
          this.feedValues.set(details.temperatureFeedId, 0.0);
          break;
      }
    }
    this.feedList = Array.from(this.feedValues.keys());
    this.queryString = new URLSearchParams({
      ids: this.feedList.join(','),
      apikey: this.apikey,
    });

    /**
     * All cached accessories now restored from disk, remove out of date, create new, update...
     */
    this.api.on('didFinishLaunching', () => {
      log.debug('didFinishLaunching()');
      let dereg = this.deregisterAccessories.map(a => a.displayName);
      this.log.info(`Removing cached accessories no longer configured: ${dereg.join(',')}`);
      this.api.unregisterPlatformAccessories(settings.PLUGIN_NAME, settings.PLATFORM_NAME, this.deregisterAccessories);
      this.configureDevices();
      ci.setCorrectingInterval(this.updateFeedValues.bind(this), this.refresh * 1000);
    });
  }

  /**
   * Validate the configuration
   */
  validateConfiguration(config) {
    let serials = config.devices.map(a => a.serial);
    let counter = new Map();
    for (const serial of serials) {
      if (counter.has(serial)) {
        counter.set(serial, counter.get(serial) + 1);
      } else {
        counter.set(serial, 1);
      }
    }
    let duplicates = [...counter.entries()].filter(e => e[1] > 1).map(e => e[0]);
    if (duplicates.length > 0) {
      throw new Error(`Duplicate serial number(s) found: ${duplicates.join(',')}`);
    }
    for (const d of config.devices) {
      switch (d.type) {
        case 'electricity':
          if (d.voltageFeedId && d.powerFeedId && d.energyFeedId) break;
          throw new Error(`Missing electricity feedId(s) in device '${d.name}'`);
        case 'energy':
          if (d.energyFeedId) break;
          throw new Error(`Missing energy feedId(s) in device '${d.name}'`);
        case 'temperature':
          if (d.temperatureFeedId) break;
          throw new Error(`Missing temperature feed Id in device '${d.name}'`);
        default:
          throw new Error(`Invalid type '${d.type}' in device '${d.name}'`);
      }
    }
  }
  /**
   * Invoked when homebridge restores cached accessories from disk at startup.
   * If the serial number is not in the config then add to de-register list.
   */
  configureAccessory(accessory) {
    this.log.info('Loading accessory from cache:', accessory.displayName);
    var found;
    try {
      found = false; // this.config.devices.find(a => a.serial === accessory.context.details.serial);
    } catch (error) {
      this.log.error(`Loading accessory ${accessory.displayName} from cache: ${error}`);
      found = false;
    }
    if (found) {
      this.cachedAccessories.push(accessory);
    } else {
      this.deregisterAccessories.push(accessory);
    }
  }

  /**
   * Go through the configuration, see which ones were cached (based on serial number) and
   * register new ones. Since rehydration doesn't bring back the handler create for all
   */
  configureDevices() {
    this.log.debug(`configureDevices(): ${this.config.devices.length} devices)}`);

    for (const device of this.config.devices) {
      this.log.debug(`configuring: ${JSON.stringify(device)}`);

      const uuid = this.api.hap.uuid.generate(device.serial);

      let accessory = this.cachedAccessories.find(accessory => accessory.UUID === uuid);

      if (accessory) {
        this.log.info('Rehydrating cached accessory:', accessory.displayName);
      } else {
        this.log.info('Creating new accessory:', device.name);
        // eslint-disable-next-line new-cap
        accessory = new this.api.platformAccessory(device.name, uuid);
        this.api.registerPlatformAccessories(settings.PLUGIN_NAME, settings.PLATFORM_NAME, [accessory]);
      }
      // Set the config details, this also updates cached details in case they've changed
      accessory.context.details = device;
      // eslint-disable-next-line no-new
      let handler = new EmonCMSPlatformAccessory(this, accessory);
      this.accessories.set(accessory, handler);
    }
  }
  async updateFeedValues() {

    let current_time = moment().unix();
    let timeDelta = 0;
    if (this.timeLastUpdate > 0) {
      timeDelta = current_time - this.timeLastUpdate;
    }
    this.log.debug(`updateFeedValues(): ${this.accessories.size} accessories`);
    try {
      let fetchURL = `${this.EmonURL}/feed/fetch.json?${this.queryString}`;
      this.log.debug(`Requesting feed list from EmonCMS at ${fetchURL} ...`);
      const response = await fetch(fetchURL);
      if (!response.ok) {
        throw new Error(`HTTP error: ${response.status}`);
      }
      var data = await response.json();

    } catch (error) {
      this.log.error(`updateFeedValues: ${error}`);
      return;
    }
    for (let [index, value] of data.entries()) {
      this.feedValues.set(this.feedList[index], value);
    };
    this.log.debug(`feedValues: ${JSON.stringify([...this.feedValues])}`);
    for (const [accessory, handler] of this.accessories.entries()) {
      this.log.debug(`setting values in accessory: ${accessory.displayName}`);
      // FakeGato creates a service with the name suffixed " History"
      let historyService = accessory.getService(`${accessory.displayName} History`);
      let entry, energy, power, energyDelta;
      switch (accessory.context.details.type) {
        case 'temperature':
          let service = accessory.getService(this.Service.TemperatureSensor);
          let temperature = this.feedValues.get(accessory.context.details.temperatureFeedId);
          service.updateCharacteristic(this.Characteristic.CurrentTemperature, temperature);
          entry = {time: current_time, temp: temperature};
          break;

        case 'energy':
          let energyService = accessory.getService(accessory.displayName);
          energy = this.feedValues.get(accessory.context.details.energyFeedId);
          this.log.debug(`ENTRY ${accessory.context.details.type}: ${energy.toFixed(2)}, ` +
            `lastEnergy: ${handler.lastEnergy.toFixed(2)}, energySLR: ${handler.energySLR.toFixed(2)}`);
          if (timeDelta > 0) { // calculate the power consumption (W) as an average during the last interval
            energyDelta = energy - handler.lastEnergy;
            power = Math.round((energyDelta * 1000) * (3600 / timeDelta));
            if (accessory.context.details.logfile) {
              var msg = `${current_time},${timeDelta},${energy},${energyDelta.toFixed(2)},${power.toFixed()}\n`;
              fs.appendFile(accessory.context.details.logfile, msg, (err) => {
                if (err) this.log.warn(`failed to append entry to ${accessory.context.details.logfile}`);
              });
            }
          } else { // can't calculate
            power = 0.0;
            energyDelta = 0.0;
          }
          this.log.debug(`delta T: ${timeDelta}, delta E: ${energyDelta.toFixed(2)}, ` +
            `calculated P: ${power.toFixed()}`);
          handler.lastEnergy = energy;
          handler.energySLR += energyDelta;
          energyService.updateCharacteristic('Power', power);
          energyService.updateCharacteristic('Energy', handler.energySLR);
          entry = {time: current_time, power: power};
          break;

        case 'electricity':
          let electricityService = accessory.getService(accessory.displayName);
          power = this.feedValues.get(accessory.context.details.powerFeedId);
          energy = this.feedValues.get(accessory.context.details.energyFeedId);
          this.log.debug(`ENTRY ${accessory.context.details.type}: ${energy.toFixed(2)}, ` +
            `lastEnergy: ${handler.lastEnergy.toFixed(2)}, energySLR: ${handler.energySLR.toFixed(2)}`);
          handler.lastEnergy = energy;
          energyDelta = power * timeDelta / 3600 / 1000;
          this.log.debug(`delta T: ${timeDelta}, delta E: ${energyDelta.toFixed(4)}, ` +
            `measured P: ${power.toFixed()}`);
          handler.energySLR += energyDelta;
          let voltage = this.feedValues.get(accessory.context.details.voltageFeedId);
          electricityService.updateCharacteristic('Voltage', voltage);
          electricityService.updateCharacteristic('Power', power);
          electricityService.updateCharacteristic('Energy', handler.energySLR);
          entry = {time: current_time, power: power};
          break;
      }
      if (accessory.context.details.type.match(/^(electricity|energy)$/)) {
        if (historyService.isHistoryLoaded()) {
          handler.ExtraPersistedData = historyService.getExtraPersistedData();
          if (handler.ExtraPersistedData !== undefined) {
            handler.energySLR = handler.ExtraPersistedData.totalenergy + energyDelta;
            historyService.setExtraPersistedData(
              { totalenergy: handler.energySLR, lastReset: handler.ExtraPersistedData.lastReset });
          } else {
            this.log.debug(`setting extra persisted data for ${accessory.displayName}`);
            handler.energySLR += energyDelta;
            historyService.setExtraPersistedData({ totalenergy: handler.energySLR, lastReset: 0 });
          }
        } else {
          this.log.debug(`history service for ${accessory.displayName} is NOT loaded`);
          handler.energySLR += energyDelta;
        }
        this.log.debug(`EXIT ${accessory.context.details.type}: ${energy.toFixed(2)}, ` +
          `lastEnergy: ${handler.lastEnergy.toFixed(2)}, energySLR: ${handler.energySLR.toFixed(2)}`);
      }
      historyService.addEntry(entry);

      this.log.debug(`Added history entry: ${JSON.stringify(entry)}`);
    }
    this.timeLastUpdate = current_time;
  }
}
exports.EmonCMS = EmonCMS;
