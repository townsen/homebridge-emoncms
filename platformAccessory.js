'use strict';

const version = require('./package.json').version;
const settings = require('./settings');

/**
 * Platform Accessory
 * An instance of this class is created for each accessory your platform registers
 * Each accessory may expose multiple services of different service types.
 */
class EmonCMSPlatformAccessory {

  constructor(platform, accessory) {

    this.platform = platform;
    this.accessory = accessory;
    this.log = platform.log;

    this.accessorytype = accessory.context.details.type;
    this.serial = accessory.context.details.serial;
    this.displayName = accessory.context.details.name;

    this.log.debug(`EmonCMSPlatformAccessory: constructor(): ${JSON.stringify(accessory.context.details)}`);
    this.log.debug(`existing services: ${accessory.services.map(x => x.UUID)}`);

    this.lastReset = 0; // for energy and electricity devices
    this.lastEnergy = 0.0; // the last energy reading from EmonCMS
    this.energySLR = 0.0; // accumulate our own energy so we can reset it

    // An information service is already present in the accessory (!?) so just set it...
    //
    this.accessory.getService(platform.Service.AccessoryInformation)
      .setCharacteristic(platform.Characteristic.Manufacturer, 'OpenEnergyMonitor')
      .setCharacteristic(platform.Characteristic.Model, `EmonCMS-${this.accessorytype}`)
      .setCharacteristic(platform.Characteristic.FirmwareRevision, version)
      .setCharacteristic(platform.Characteristic.SerialNumber, this.serial);

    // When restoring from cache the rehydrated history service barfs on addEntry(), so always create...
    //
    this.historyService = accessory.services.find(s => s.UUID === settings.uuidFakeGatoHistoryService);
    if (this.historyService) {
      this.log.debug('Removing existing History Service');
      accessory.removeService(this.historyService);
      this.historyService = undefined;
    }

    switch (this.accessorytype) {
      case 'electricity':
      case 'energy':
        this.ExtraPersistedData = {};
        this.powerFeedId = accessory.context.details.powerFeedId;
        this.energyFeedId = accessory.context.details.energyFeedId;
        this._EvePowerConsumption = new platform.Characteristic(
          'Power',
          'E863F10D-079E-48FF-8F27-9C2605A29F52',
          {
            format: platform.Characteristic.Formats.UINT16,
            unit: 'Watts',
            maxValue: 100000,
            minValue: 0,
            minStep: 1,
            perms: [platform.Characteristic.Perms.READ, platform.Characteristic.Perms.NOTIFY],
          },
        );

        this._EveTotalConsumption = new platform.Characteristic(
          'Energy',
          'E863F10C-079E-48FF-8F27-9C2605A29F52',
          {
            format: platform.Characteristic.Formats.FLOAT,
            unit: 'kWh',
            maxValue: 1000000000,
            minValue: 0,
            minStep: 0.001,
            perms: [platform.Characteristic.Perms.READ, platform.Characteristic.Perms.NOTIFY],
          },
        );

        this._ResetTotal = new platform.Characteristic(
          'Reset',
          'E863F112-079E-48FF-8F27-9C2605A29F52',
          {
            format: platform.Characteristic.Formats.UINT32,
            perms: [platform.Characteristic.Perms.READ,
              platform.Characteristic.Perms.NOTIFY,
              platform.Characteristic.Perms.WRITE],
          },
        );
        this._ResetTotal.on('set', (value, callback) => { // toggle between set-to-zero and back to EmonCMS
          if (Math.round(this.energySLR) !== Math.round(this.lastEnergy)) {
            this.energySLR = this.lastEnergy;
          } else {
            this.energySLR = 0.0;
          }
          this.log.debug(`Resetting total energy to ${this.energySLR} kWh`);
          this.service.updateCharacteristic('Energy', this.energySLR);
          this.lastReset = value;
          this.historyService.setExtraPersistedData({ totalenergy: this.energySLR, lastReset: this.lastReset });
          callback(null);
        }).on('get', (callback) => {
          this.ExtraPersistedData = this.historyService.getExtraPersistedData();
          if (this.ExtraPersistedData !== undefined)
            this.lastReset = this.ExtraPersistedData.lastReset;
          callback(null, this.lastReset);
        });

        if (this.accessorytype === 'electricity') {
          this.voltageFeedId = accessory.context.details.voltageFeedId;
          this._EveVoltage = new platform.Characteristic(
            'Voltage',
            'E863F10A-079E-48FF-8F27-9C2605A29F52',
            {
              format: platform.Characteristic.Formats.FLOAT,
              unit: 'Volt',
              maxValue: 1000,
              minValue: 0,
              minStep: 0.1,
              perms: [platform.Characteristic.Perms.READ, platform.Characteristic.Perms.NOTIFY],
            },
          );
        }
        this.service = accessory.services.find(service => service.UUID === settings.uuidEveEnergy);
        if (this.service === undefined) {
          this.log.debug('Creating new energy service...');
          this.service = new platform.Service(this.displayName, settings.uuidEveEnergy);
          this.service.addCharacteristic(this._EvePowerConsumption);
          this.service.addCharacteristic(this._EveTotalConsumption);
          this.service.addCharacteristic(this._ResetTotal);
          if (this.accessorytype === 'electricity') {
            this.service.addCharacteristic(this._EveVoltage);
          }
          accessory.addService(this.service);
        } else {
          this.log.debug('Using existing energy service ...');
        }
        if (this.historyService === undefined) {
          this.log.debug('Creating new history service (energy) ...');
          this.historyService = new this.platform.FakeGatoHistory('energy', this, {storage: 'fs'});
          accessory.addService(this.historyService);
        } else {
          this.log.debug('Using existing history service ...');
        }
        break;

      case 'temperature':
        this.service = this.accessory.getService(this.platform.Service.TemperatureSensor);
        if (this.service === undefined) {
          this.service = new this.platform.Service.TemperatureSensor(this.displayName);
          this.service
            .getCharacteristic(this.platform.Characteristic.CurrentTemperature)
            .setProps({
              minValue: -100,
              maxValue: 100,
            });
          accessory.addService(this.service);
        } else {
          this.log.debug('Using existing TemperatureSensor service ...');
        }
        if (this.historyService === undefined) {
          this.log.debug('Creating new history service (weather) ...');
          this.historyService = new this.platform.FakeGatoHistory('weather', this, {storage: 'fs'});
          accessory.addService(this.historyService);
        } else {
          this.log.debug('Using existing history service ...');
        }
        break;

      default:
        throw new Error('Wrong accessory type!');
    }
    this.service.log = this.log;
    this.log.debug('Exiting EmonCMSPlatformAccessory constructor');

  }

}
exports.EmonCMSPlatformAccessory = EmonCMSPlatformAccessory;
