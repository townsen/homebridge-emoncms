# Change Log

## 0.10.1 (2023-02-07)
### Changes
* Allow negative values (for temperature feeds)

## 0.9.5 (2023-01-14)
### Changes
* Implement Reset Count function for electricity and energy devices

## 0.9.4 (2023-01-13)
### Changes
* Add logfile option to aid debugging energy to power calculation

## 0.9.3 (2023-01-12)
### Changes
* Delay updating values until configuration built (race condition)

## 0.9.2 (2023-01-12)
### Changes
* Catch erros when deleting cached accessories no longer configured

## 0.9.1 (2023-01-12)
### Changes
* Remove feed Id limit (64) in config.schema
* Catch erros when reloading from cache

## 0.9 (2023-01-12)
### Changes
* Add support for generic energy meters using pulse counting
* Use the term 'Energy' instead of 'Cumulative Power' (breaking change!)

## 0.8.5 (2023-01-02)
### Changes
Use 'node-fetch' version 2 package:
* Gives meaningful message if URL of EmonCMS is incorrect
* Allows use of earlier versions of NodeJS

## 0.8.4 (2022-12-30)
### Changes
* Use 'electricity' for device type as it includes voltage.
* Set negative measured power values to zero!
  This avoids the log message: 'characteristic was supplied illegal value'

## 0.8.2 (2022-12-29)
### Changes
* Correctly handle cached accessories, remove when serial number changes
* Update details of cached accessories if they change
* Use 'devices' instead of 'accessories' in config.json

## 0.8.1 (2022-12-29)
### Changes
* Fixed Information Service bugs

## 0.8.0 (2022-12-28)
### Changes
* FakeGato history now working

## 0.7.0 (2022-12-28)
### Changes
* Now a platform plugin

## 0.2.1 (2022-12-24)
### Changes
* Record Temperature measurement (optional)
* Used modern Javascript class syntax (new) instead of wacky function stuff.
* Tidied up README.md

## 0.2.0 (2022-12-24)
### Changes
* Added Temperature measurement (optional)
* Used modern Javascript class syntax (new) instead of wacky function stuff.
* Tidied up README.md

## 0.1.1 (2022-12-17)
### Changes
* Added package-lock.json to git repo
* Updated config.schema.json
* Tidied up README.md

## 0.1.0 (2022-12-17)
### Changes
* First working version, no temperature support, one channel only

## 0.0.9 (2022-12-15)
### Changes
* Initial version
