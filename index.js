'use strict';

const settings = require('./settings');
const platform = require('./platform');
module.exports = (api) => {
  api.registerPlatform(settings.PLATFORM_NAME, platform.EmonCMS);
};
